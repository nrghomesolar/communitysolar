

1.  Execute the sp_replicate.sql  to Replicate the tables for Facility_Production__c and  Solar_Production__c from Salesforce.

2. Update table definitions for 
	Solar_Production__c_Upsert, 
	Faciity_Production__c_Upsert,
	Xcel_Monthly_Owner_Report_Extract,
	Xcel_Data_Transform,
	Xcel_Subscriber_Allocation_Summary_Extract,
	Xcel_Allocation_Staging
using their corresponding sql files like 

	Solar_Production__c_Upsert.sql, 
	Faciity_Production__c_Upsert.sql,
	Xcel_Monthly_Owner_Report_Extract.sql,
	Xcel_Data_Transform.sql,
	Xcel_Subscriber_Allocation_Summary_Extract.sql,
	Xcel_Allocation_Staging.sql

3. Update stored procedure SP_Xcel_Transform using SP_Xcel_Transform.sql
4. Deploy the CS_PPA_Billing package
