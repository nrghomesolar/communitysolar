Deployment Instructions:

1. Execute statement in "SP_Xcel_Transform 06162017.sql" to alter SP_Xcel_Transform procedure
2. Deploy package


Change log:
1. SR-0012919: Added support for creating solar productions if an allocation is in amended or cancelled stage
2. SR-0012919: Match allocation size to 1st decimal place to avoid mismatch due to rounding
