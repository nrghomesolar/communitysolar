﻿USE [NRGHomeSolarSF]
GO

/****** Object:  StoredProcedure [dbo].[SP_Xcel_Transform]    Script Date: 5/15/2018 6:55:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[SP_Xcel_Transform] 
AS 
    SET NOCOUNT ON;

	/*
	Delete all records from monthly owner report which are not for latest period by garden
	*/
	DELETE mor From Xcel_Monthly_Owner_Report_Extract  mor
	join (select [Garden ID], Max([Bill Period]) as [Max Bill Period] From Xcel_Monthly_Owner_Report_Extract Group By [Garden ID])  cbp
	on mor.[Garden ID] = cbp.[Garden ID]
	Where mor.[Bill Period] < cbp.[Max Bill Period]

	/*
	 Merge data extacts
	*/
    INSERT INTO [Xcel_Data_Transform]
	Select	[Premises Number],
			[Debtor Number],
			[Garden ID],
			RTrim([Customer Name]),
			[Garden Name], 
			[Payment Type],
			Sum([Tariff Rate]) as [Tariff Rate],
			[Allocation Size],
			[Status],
			[Bill Period],
			Sum([Bill Credit]) as [Bill Credit],
			[Production kWh]
	from (
			SELECT
			COALESCE (allocation.[Premises Number], monthly.[Premises Number]) AS [Premises Number],
			COALESCE (allocation.[Debtor Number], monthly.[Debtor Number]) AS [Debtor Number],
			COALESCE (allocation.[Garden ID], monthly.[Garden ID]) AS [Garden ID],
			[Customer Name], [Garden Name], [Payment Type],
			CAST(([Tariff Rate]) AS money) AS [Tariff Rate],
			(Case When [Payment Type] = 'S' Then allocation.[Allocation Size]
					else  monthly.[Allocation Size] End ) AS [Allocation Size],
			[Status],
			CAST([Bill Period]+'-01' AS date) AS [Bill Period],
			CAST(([Bill Credit]) AS money) AS [Bill Credit],
			[Production kWh]
			FROM Xcel_Subscriber_Allocation_Summary_Extract allocation
			FULL OUTER JOIN Xcel_Monthly_Owner_Report_Extract monthly
				ON monthly.[Premises Number] = allocation.[Premises Number]
					--AND monthly.[Debtor Number] = allocation.[Debtor Number]
					AND monthly.[Garden ID] = allocation.[Garden ID]					
		) as temp
	Group by [Premises Number],	[Debtor Number],[Garden ID],[Customer Name],[Garden Name],[Status],	[Payment Type],	[Allocation Size],[Bill Period],[Production kWh]


	/*
	Populate Allocation Staging Table
	*/
		
	INSERT INTO [Xcel_Allocation_Staging]
	SELECT
		COALESCE (xcel.[Premises Number], sf.Premises_Number__c) AS [Premises Number],
		[Debtor Number],
		sf.[Garden ID],
		sf.[Customer Name], sf.[Garden Name], [Payment Type],
		[Tariff Rate],
		COALESCE (xcel.[Allocation Size], sf.Sold_KW__c) AS [Allocation Size],
		[Status],
		COALESCE ([Bill Period],cbp.[Current Bill Period]) As [Bill Period],
		[Bill Credit],
		[Production kWh],
		sf.[Facility SF ID],
		--NULL AS [Facility Production_Start_Date__c],
		sf.Production_Start_Date__c AS [Facility Production_Start_Date__c],
		sf.[Facility Status],
		sf.[Cancelled_Date__c],
		sf.[Lease_ID__c],
		sf.[Allocation SF ID],
		sf.[Paper_Invoice_Requested__c],
		sf.[Percent_of_Production__c],
		sf.[PPA_Rate__c],
		Null as [PPA Subsrciption Rate],
		sf.[Enrolled_With_Utility__c],
		sf.Start_Date__c AS [Allocation Start Date],
		[Allocation SF Status],
		sf.[Transfer_Status__c],
		sf.[Utility_Account_Number__c]				
	FROM Xcel_Data_Transform xcel
		FULL OUTER JOIN
			(SELECT [Cancelled_Date__c], [Lease_ID__c], f.Id AS [Facility SF ID], f.Status__c AS [Facility Status],
				[Paper_Invoice_Requested__c], [Percent_of_Production__c], [PPA_Rate__c], Premises_Number__c, Sold_KW__c,
				[Enrolled_With_Utility__c], Start_Date__c, fa.Id AS [Allocation SF ID], fa.Status__c AS [Allocation SF Status], [Transfer_Status__c], 
				[Utility_Account_Number__c], f.SRC_Garden_Number__c AS [Garden ID], f.Name AS [Garden Name], f.Production_Start_Date__c, fa.Conga_Customers_Concat__c as [Customer Name],
				[Monthly_Price_Year_2__c]
			FROM Facility_Allocation__c fa JOIN Facility__c f ON fa.Facility__c = f.Id
			Where f.SRC_Garden_Number__c in (Select distinct [Garden Id] from [dbo].[Xcel_Monthly_Owner_Report_Extract] )		
			) sf
			ON LTRIM(RTRIM(xcel.[Premises Number])) collate DATABASE_DEFAULT = LTRIM(RTRIM(sf.Premises_Number__c)) collate DATABASE_DEFAULT
				AND (xcel.[Allocation Size] is null or Round(xcel.[Allocation Size],1,1) = Round(sf.Sold_KW__c,1,1)	)
				and xcel.[Garden ID] = sf.[Garden ID]
		left join (select [Garden ID], Max(CAST([Bill Period]+'-01' AS date)) as [Current Bill Period] From Xcel_Monthly_Owner_Report_Extract Group By [Garden ID]) cbp
		on sf.[Garden ID] = cbp.[Garden ID]
	WHERE
		(sf.[Allocation SF Status] in ('Sold','Amended','Cancelled' )AND sf.[Facility Status] LIKE 'In Production') AND 
		sf.[PPA_Rate__c] != 0
	ORDER BY [Premises Number]


	/* 
		Populate Subscription Rate in allocation staging table based on the production year 
	*/

	update xas
	set    [PPA Subscription Rate] = (Case DATEDIFF ( yy , [Bill Period] , [Allocation Start Date] ) + 1  
		   When 1 Then fa.Monthly_Price_Year_1__c
		   When 2 Then fa.Monthly_Price_Year_2__c
		   When 3 Then fa.Monthly_Price_Year_3__c
		   When 4 Then fa.Monthly_Price_Year_4__c
		   When 5 Then fa.Monthly_Price_Year_5__c
		   When 6 Then fa.Monthly_Price_Year_6__c
		   When 7 Then fa.Monthly_Price_Year_7__c
		   When 8 Then fa.Monthly_Price_Year_8__c
		   When 9 Then fa.Monthly_Price_Year_9__c
		   When 10 Then fa.Monthly_Price_Year_10__c
		   When 11 Then fa.Monthly_Price_Year_11__c
		   When 12 Then fa.Monthly_Price_Year_12__c
		   When 13 Then fa.Monthly_Price_Year_13__c
		   When 14 Then fa.Monthly_Price_Year_14__c
		   When 15 Then fa.Monthly_Price_Year_15__c
		   When 16 Then fa.Monthly_Price_Year_16__c
		   When 17 Then fa.Monthly_Price_Year_17__c
		   When 18 Then fa.Monthly_Price_Year_18__c
		   When 19 Then fa.Monthly_Price_Year_19__c
		   When 20 Then fa.Monthly_Price_Year_20__c
		   When 21 Then fa.Monthly_Price_Year_21__c
		   When 22 Then fa.Monthly_Price_Year_22__c
		   When 23 Then fa.Monthly_Price_Year_23__c
		   When 24 Then fa.Monthly_Price_Year_24__c
		   else fa.Monthly_Price_Year_25__c
		   End)
	From [Xcel_Allocation_Staging] xas
	inner join Facility_Allocation__c fa
	on xas.[Allocation SF ID] = fa.Id	
	Where xas.[Bill Period] is not null and xas.[Allocation Start Date] is not null







GO