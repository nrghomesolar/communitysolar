﻿USE [NRGHomeSolarSF]
GO

/****** Object:  Table [dbo].[Xcel_Allocation_Staging]    Script Date: 5/10/2017 4:23:52 PM ******/
DROP TABLE [dbo].[Xcel_Allocation_Staging]
GO

/****** Object:  Table [dbo].[Xcel_Allocation_Staging]    Script Date: 5/10/2017 4:23:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Xcel_Allocation_Staging](
	[Premises Number] [char](16) NULL,
	[Debtor Number] [char](16) NULL,
	[Garden ID] [char](16) NULL,
	[Customer Name] [nchar](250) NULL,
	[Garden Name] [nchar](250) NULL,
	[Payment Type] [nchar](5) NULL,
	[Tariff Rate] [money] NULL,
	[Allocation Size] [decimal](16, 3) NULL,
	[Status] [nchar](10) NULL,
	[Bill Period] [date] NULL,
	[Bill Credit] [money] NULL,
	[Production kWh] [decimal](15, 2) NULL,
	[Facility SF ID] [nchar](18) NULL,
	[Facility Production_Start_Date__c] [datetime2](7) NULL,
	[Facility Status] [nvarchar](255) NULL,
	[Cancelled_Date__c] [datetime2](7) NULL,
	[Lease_ID__c] [nvarchar](100) NULL,
	[Allocation SF ID] [nchar](18) NULL,
	[Paper_Invoice_Requested__c] [varchar](5) NULL,
	[Percent_of_Production__c] [decimal](18, 2) NULL,
	[PPA_Rate__c] [decimal](18, 4) NULL,
	[PPA Subscription Rate] [decimal](18, 4) NULL,
	[Enrolled_With_Utility__c] [varchar](5) NULL,
	[Allocation Start Date] [datetime2](7) NULL,
	[Allocation SF Status] [nvarchar](255) NULL,
	[Transfer_Status__c] [nvarchar](255) NULL,
	[Utility_Account_Number__c] [nvarchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

