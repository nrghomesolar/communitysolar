﻿USE [NRGHomeSolarSF]
GO

/****** Object:  Table [dbo].[Facility_Production__c_Upsert]    Script Date: 5/10/2017 4:22:37 PM ******/
DROP TABLE [dbo].[Facility_Production__c_Upsert]
GO

/****** Object:  Table [dbo].[Facility_Production__c_Upsert]    Script Date: 5/10/2017 4:22:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Facility_Production__c_Upsert](
	[Id] [nchar](18) NULL,
	[Name] [nvarchar](80) NULL,
	[PI_Server_Id__c] [nvarchar](100) NULL,
	[Facility__c] [nvarchar](18) NULL,
	[Net_KwH__c] [decimal](18, 3) NULL,
	[Period_Start__c] [datetime2](7) NOT NULL,
	[Period_End__c] [datetime2](7) NOT NULL,
	[Start_KwH__c] [decimal](18, 3) NULL,
	[End_KwH__c] [decimal](18, 3) NULL,
	[Calculated_Sum__c] [varchar](5) NULL,
	[Unique_Identifier__c] [nvarchar](100) NULL,
	[Error] [nvarchar](255) NULL,
	[Virtual_Net_Metering_Credits__c] [decimal](18, 2) NULL,
	[Period_Year__c] [nvarchar](40) NULL,
	[Month_Count__c] [decimal](18, 0) NULL,
	[Unallocated_Credits__c] [decimal](18, 2) NULL,
	[Subscribed_Allocated_Production_kWh__c] [decimal](18, 4) NULL,
	[Total_Solar_Productions_kWh__c] [decimal](18, 4) NULL,
	[Unsubscribed_Unallocated_Production_kWh__c] [decimal](18, 4) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


