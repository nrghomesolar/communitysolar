﻿USE [NRGHomeSolarSF]
GO

/****** Object:  Table [dbo].[Xcel_Monthly_Owner_Report_Extract]    Script Date: 5/10/2017 4:23:02 PM ******/
DROP TABLE [dbo].[Xcel_Monthly_Owner_Report_Extract]
GO

/****** Object:  Table [dbo].[Xcel_Monthly_Owner_Report_Extract]    Script Date: 5/10/2017 4:23:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Xcel_Monthly_Owner_Report_Extract](
	[Payment Type] [nchar](5) NOT NULL,
	[Debtor Number] [char](16) NULL,
	[Premises Number] [char](16) NULL,
	[Production kWh] [decimal](15, 2) NULL,
	[Tariff Rate] [money] NULL,
	[Bill Credit] [money] NULL,
	[Garden ID] [char](16) NULL,
	[Allocation Size] [decimal](15, 2) NULL,
	[Bill Period] [nchar](8) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


