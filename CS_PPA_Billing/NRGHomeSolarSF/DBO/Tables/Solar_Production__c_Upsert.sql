﻿

USE [NRGHomeSolarSF]
GO

/****** Object:  Table [dbo].[Solar_Production__c_Upsert]    Script Date: 5/10/2017 4:22:06 PM ******/
DROP TABLE [dbo].[Solar_Production__c_Upsert]
GO

/****** Object:  Table [dbo].[Solar_Production__c_Upsert]    Script Date: 5/10/2017 4:22:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Solar_Production__c_Upsert](
	[Id] [nchar](18) NULL,
	[Facility_Allocation__c] [nchar](18) NULL,
	[Facility_Production__c] [nchar](18) NULL,
	[Lease_Id__c] [nvarchar](100) NULL,
	[Net_KwH__c] [decimal](18, 3) NULL,
	[Period_End__c] [datetime2](7) NULL,
	[Period_Start__c] [datetime2](7) NULL,
	[Period_Year__c] [int] NULL,
	[RecordTypeId] [nchar](18) NULL,
	[Status__c] [nvarchar](250) NULL,
	[Subscription_Rate__c] [decimal](18, 5) NULL,
	[Unique_Identifier__c] [nvarchar](100) NULL,
	[Virtual_Net_Metering_Credits__c] [decimal](18, 2) NULL,
	[Error] [nvarchar](255) NULL
) ON [PRIMARY]

GO


