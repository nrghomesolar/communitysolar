﻿USE [NRGHomeSolarSF]
GO

/****** Object:  Table [dbo].[Xcel_Subscriber_Allocation_Summary_Extract]    Script Date: 5/10/2017 4:23:37 PM ******/
DROP TABLE [dbo].[Xcel_Subscriber_Allocation_Summary_Extract]
GO

/****** Object:  Table [dbo].[Xcel_Subscriber_Allocation_Summary_Extract]    Script Date: 5/10/2017 4:23:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Xcel_Subscriber_Allocation_Summary_Extract](
	[Premises Number] [char](16) NULL,
	[Debtor Number] [char](16) NULL,
	[Garden Name] [nchar](250) NULL,
	[Customer Name] [nchar](250) NULL,
	[Allocation Size] [decimal](15, 3) NULL,
	[Status] [nchar](10) NULL,
	[Status 2] [nchar](20) NULL,
	[Agency Agreement] [bit] NULL,
	[Low Income] [bit] NULL,
	[Low Income Verified] [bit] NULL,
	[Privacy Form] [bit] NULL,
	[Garden ID] [char](16) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


