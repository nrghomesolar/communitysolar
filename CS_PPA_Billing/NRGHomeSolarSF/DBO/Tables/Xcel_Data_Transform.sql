﻿USE [NRGHomeSolarSF]
GO

/****** Object:  Table [dbo].[Xcel_Data_Transform]    Script Date: 5/10/2017 4:23:19 PM ******/
DROP TABLE [dbo].[Xcel_Data_Transform]
GO

/****** Object:  Table [dbo].[Xcel_Data_Transform]    Script Date: 5/10/2017 4:23:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Xcel_Data_Transform](
	[Premises Number] [char](16) NULL,
	[Debtor Number] [char](16) NULL,
	[Garden ID] [char](16) NULL,
	[Customer Name] [nchar](250) NULL,
	[Garden Name] [nchar](250) NULL,
	[Payment Type] [nchar](5) NULL,
	[Tariff Rate] [money] NULL,
	[Allocation Size] [decimal](16, 3) NULL,
	[Status] [nchar](10) NULL,
	[Bill Period] [date] NULL,
	[Bill Credit] [money] NULL,
	[Production kWh] [decimal](15, 2) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



