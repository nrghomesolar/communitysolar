﻿USE [CommunitySolarDW]
GO

/****** Object:  View [dbo].[v_Facility_Allocation__c]    Script Date: 11/15/2017 10:02:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_Facility_Allocation__c]
AS
SELECT *
From [NRGHomeSolarSF].dbo.Facility_Allocation__c

GO
