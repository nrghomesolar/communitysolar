﻿USE [CommunitySolarDW]
GO

/****** Object:  View [dbo].[v_Facility_Production__c]    Script Date: 11/15/2017 10:03:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_Facility_Production__c]
AS
SELECT *
From [NRGHomeSolarSF].dbo.Facility_Production__c

GO
