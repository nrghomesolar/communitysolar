﻿USE [CommunitySolarDW]
GO

/****** Object:  View [dbo].[v_Case]    Script Date: 11/15/2017 9:59:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Case]
AS
SELECT *
From [NRGHomeSolarSF].dbo.[Case]
Where Facility_Allocation__c Is Not Null


GO
