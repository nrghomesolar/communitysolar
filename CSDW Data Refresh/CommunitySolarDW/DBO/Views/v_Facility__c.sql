﻿USE [CommunitySolarDW]
GO

/****** Object:  View [dbo].[v_Facility__c]    Script Date: 11/15/2017 10:02:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_Facility__c]
AS
SELECT *
From [NRGHomeSolarSF].dbo.Facility__c

GO


