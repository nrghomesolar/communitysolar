﻿USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[CSMAP_SalesTeam]    Script Date: 2/5/2018 9:10:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSMAP_SalesTeam](
	[Facility Sales Group] [nvarchar](80) NOT NULL,
	[Sales Team] [nvarchar](50) NOT NULL,
	[Azure Group] [nvarchar](50) NULL
) ON [PRIMARY]

GO





USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[CSMAP_SalesCompany]    Script Date: 2/5/2018 9:10:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSMAP_SalesCompany](
	[LeadCompany] [nvarchar](30) NOT NULL,
	[SalesCompany] [nvarchar](30) NOT NULL,
	[AzureGroup] [nvarchar](60) NOT NULL
) ON [PRIMARY]

GO




USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[CSMAP_SalesForecast]    Script Date: 2/5/2018 9:11:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSMAP_SalesForecast](
	[Week Ending] [datetime2](7) NOT NULL,
	[State] [nvarchar](2) NOT NULL,
	[Sales Facility Group] [nvarchar](30) NOT NULL,
	[Sales Team] [nvarchar](30) NOT NULL,
	[Month] [nvarchar](3) NOT NULL,
	[Signed Forecast kW] [numeric](5, 0) NOT NULL,
	[Sold Forecast kW] [numeric](5, 0) NOT NULL,
	[Signed Forecast Ct] [numeric](5, 0) NOT NULL,
	[Sold Forecast Ct] [numeric](5, 0) NOT NULL
) ON [PRIMARY]

GO




USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[CSMAP_NRGPlatinumAgentRoster]    Script Date: 2/5/2018 9:11:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSMAP_NRGPlatinumAgentRoster](
	[Campaign] [nvarchar](30) NOT NULL,
	[Office] [nvarchar](30) NULL,
	[ID] [nvarchar](30) NULL,
	[First Name] [nvarchar](30) NOT NULL,
	[Middle Name] [nvarchar](30) NULL,
	[Last Name] [nvarchar](30) NOT NULL,
	[Activation Date] [datetime2](7) NULL,
	[Termination Date] [datetime2](7) NULL,
	[Term Reason] [nvarchar](10) NOT NULL,
	[Full Name] [nvarchar](60) NOT NULL,
	[Unique Names] [nvarchar](60) NOT NULL
) ON [PRIMARY]

GO






