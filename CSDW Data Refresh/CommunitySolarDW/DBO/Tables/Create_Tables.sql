﻿
USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[ARR_Data]    Script Date: 11/16/2017 2:40:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ARR_Data](
	[File Date] [datetime2](7) NULL,
	[Company Code] [nvarchar](10) NULL,
	[Company Code- text] [nvarchar](50) NULL,
	[Business Partner] [nvarchar](20) NULL,
	[BP: First Name] [nvarchar](80) NULL,
	[BP: Surname] [nvarchar](80) NULL,
	[State] [nvarchar](50) NULL,
	[Contract Account] [nvarchar](20) NULL,
	[Lease Agree ID Solar] [nvarchar](50) NULL,
	[Contract Account - Created on (Key)] [date] NULL,
	[Contract Account - Payment Terms (Key)] [nvarchar](50) NULL,
	[Contract Account - Payment Terms (Text)] [nvarchar](50) NULL,
	[Total-AR] [money] NULL,
	[Current] [money] NULL,
	[Past Due] [money] NULL,
	[1-30 dpd] [money] NULL,
	[31-60 dpd] [money] NULL,
	[61-90 dpd] [money] NULL,
	[91-120 dpd] [money] NULL,
	[121-150 dpd] [money] NULL,
	[151-360 dpd] [money] NULL,
	[> 360 dpd] [money] NULL,
	[Column1] [money] NULL
) ON [PRIMARY]

GO




USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[ARR_SolterraContractAcct]    Script Date: 11/15/2017 11:02:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ARR_SolterraContractAcct](
	[seqId] [int] IDENTITY(1,1) NOT NULL,
	[Contract Account] [nvarchar](20) NOT NULL,
	[Type] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[seqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[ARR_SolterraLeaseID]    Script Date: 11/15/2017 11:02:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ARR_SolterraLeaseID](
	[seqId] [int] IDENTITY(1,1) NOT NULL,
	[Lease Id] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[seqId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[Config]    Script Date: 11/15/2017 11:03:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Config](
	[Object Name] [nvarchar](50) NULL,
	[Object Type] [nvarchar](50) NULL,
	[Config Name] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL,
	[Data Type] [nvarchar](15) NULL,
	[Value] [nvarchar](255) NULL,
 CONSTRAINT [uq_config] UNIQUE NONCLUSTERED 
(
	[Object Name] ASC,
	[Config Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[CSMAP_Class]    Script Date: 11/15/2017 11:03:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSMAP_Class](
	[netMeterID] [int] IDENTITY(1,1) NOT NULL,
	[Net Metering Class] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[netMeterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[CSMAP_FacilityGroup]    Script Date: 11/15/2017 11:03:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CSMAP_FacilityGroup](
	[groupID] [int] IDENTITY(1,1) NOT NULL,
	[FacilityName] [nvarchar](80) NOT NULL,
	[FacilityGroup] [nvarchar](80) NOT NULL,
	[Sales Facility Group] [nvarchar](80) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[groupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[Date Master]    Script Date: 11/15/2017 11:03:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Date Master](
	[IdDate] [numeric](8, 0) NOT NULL,
	[Date] [date] NOT NULL,
	[Year] [numeric](4, 0) NOT NULL,
	[Quarter Full] [varchar](15) NOT NULL,
	[Quarter] [varchar](5) NOT NULL,
	[Month Full] [varchar](20) NOT NULL,
	[Month Name] [varchar](10) NOT NULL,
	[Month Number] [numeric](2, 0) NOT NULL,
	[Month Day] [numeric](2, 0) NOT NULL,
	[Week Number] [numeric](2, 0) NOT NULL,
	[Day of Year Number] [numeric](3, 0) NOT NULL,
	[Day of Week Number] [numeric](2, 0) NOT NULL,
	[Day of Week] [varchar](3) NOT NULL,
	[Week Of] [date] NOT NULL,
	[Week Ending] [date] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[DateRequest]    Script Date: 11/15/2017 11:03:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DateRequest](
	[IdDate] [nvarchar](255) NULL,
	[Date] [datetime2](7) NULL,
	[Year] [float] NULL,
	[Quarter Full] [nvarchar](255) NULL,
	[Quarter] [nvarchar](255) NULL,
	[Month Full] [nvarchar](255) NULL,
	[Month Name] [nvarchar](255) NULL,
	[Month Number] [float] NULL,
	[Month Day] [float] NULL,
	[Week Number] [float] NULL,
	[Day of Year Number] [nvarchar](255) NULL,
	[DayOfWeekNum] [float] NULL,
	[DayOfWeek] [nvarchar](255) NULL,
	[Week Of] [datetime2](7) NULL,
	[Week Ending] [datetime2](7) NULL
) ON [PRIMARY]

GO
USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[DBOP_Load Zone]    Script Date: 11/15/2017 11:04:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DBOP_Load Zone](
	[loadZoneID] [int] IDENTITY(1,1) NOT NULL,
	[DBOP Load Zone] [nvarchar](20) NOT NULL,
	[Salesforce Load Zone] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[loadZoneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[DBOP_StateLoadZones]    Script Date: 11/15/2017 11:04:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DBOP_StateLoadZones](
	[stateZoneID] [int] IDENTITY(1,1) NOT NULL,
	[State] [nvarchar](20) NOT NULL,
	[Utility] [nvarchar](20) NOT NULL,
	[Load Zone] [nvarchar](255) NULL,
	[Class] [numeric](2, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[stateZoneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[DBOP_Utility]    Script Date: 11/15/2017 11:04:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DBOP_Utility](
	[utilityID] [int] IDENTITY(1,1) NOT NULL,
	[DBOP Utility] [nvarchar](20) NOT NULL,
	[Salesforce Utility] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[utilityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CommunitySolarDW]
GO

/****** Object:  Table [dbo].[TimeRequest]    Script Date: 11/15/2017 11:04:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TimeRequest](
	[IdTime] [nvarchar](255) NULL,
	[Time] [datetime2](7) NULL,
	[Hour] [float] NULL,
	[Minute] [float] NULL,
	[TimeOfDay] [nvarchar](255) NULL,
	[IsEarlyMorning] [bit] NULL,
	[IsMorning] [bit] NULL,
	[IsAfternoon] [bit] NULL,
	[IsLateAfternoonOrEvening] [bit] NULL,
	[IsDuringWorkHours] [bit] NULL
) ON [PRIMARY]

GO





