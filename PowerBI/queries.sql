1. Schedule regular Refresh for UserRole,Allocation_Package_Deficiency__c tables if not available already.

use ETLAdmin
go
select * from TablesToSync 
where tablename in ('UserRole','Allocation_Package_Deficiency__c') order by 1

insert into TablesToSync values('UserRole')
insert into TablesToSync values('Allocation_Package_Deficiency__c')
insert into TablesToSync values('Facility__c')

select * from TablesToSync 
where tablename in (
'lead'
,'Facility__c'
,'Facility_Allocation__c'
,'Allocation_Package__c'
,'USER'
,'UserRole'
,'Allocation_Package_Deficiency__c'
)


2.Replicate the copies of tbales from salesforce.

USE NRGHomeSolarSF
GO
exec sf_refresh 'SALESFORCE', 'UserRole', 'Yes';
exec sf_refresh 'SALESFORCE', 'Allocation_Package_Deficiency__c', 'Yes';
exec sf_refresh 'SALESFORCE', 'Facility__c', 'Yes';
exec sf_refresh 'SALESFORCE', 'Facility_Allocation__c', 'Yes';


