﻿USE [NRGHomeSolarSF]
GO

/****** Object:  View [dbo].[v_CSVR_Allocation_Package_Deficiency__c]    Script Date: 12/22/2017 4:32:35 PM ******/
DROP VIEW [dbo].[v_CSVR_Allocation_Package_Deficiency__c]
GO

/****** Object:  View [dbo].[v_CSVR_Allocation_Package_Deficiency__c]    Script Date: 12/22/2017 4:32:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[v_CSVR_Allocation_Package_Deficiency__c]
AS
SELECT        TOP (100) PERCENT Allocation_Package__c, CreatedById, CreatedDate, Date_Closed__c, Details__c, LastModifiedDate, Status__c, Type__c, Id
FROM            dbo.Allocation_Package_Deficiency__c
WHERE        (CreatedDate > DATEADD(YEAR, - 1, GETDATE()))


GO


