﻿USE [NRGHomeSolarSF]
GO

/****** Object:  View [dbo].[v_CSVR_User]    Script Date: 11/28/2017 9:17:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[v_CSVR_User]
AS
SELECT	CompanyName,
		CreatedById,
		CS_Employee__c,
		EmployeeNumber,
		Id,
		IsActive,
		LastModifiedDate,
		Manager_Supervisor__c,
		ManagerId,
		ManagerId__c,
		Name,
		ProfileId,
		SAP_ID__c,
		Username,
		UserRoleId 
FROM	dbo.[USER] 
Where	Id in (Select distinct CreatedById from Lead) or 
		Id in (Select distinct SalesPerson__c From Facility_Allocation__c)


GO


