﻿USE [NRGHomeSolarSF]
GO

/****** Object:  View [dbo].[v_CSVR_Allocation_Package__c]    Script Date: 12/22/2017 4:31:00 PM ******/
DROP VIEW [dbo].[v_CSVR_Allocation_Package__c]
GO

/****** Object:  View [dbo].[v_CSVR_Allocation_Package__c]    Script Date: 12/22/2017 4:31:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[v_CSVR_Allocation_Package__c]
AS
SELECT        TOP (100) PERCENT Claimed_by_Operations__c, CreatedById, CreatedDate, Date_Approved__c, Date_Submitted_for_Approval__c, Facility_Allocation__c, Has_Lease_Ops_Approved__c, Id, LastModifiedById, 
                         LastModifiedDate, LastReferencedDate, LastViewedDate, Lease_Docs_Manually_Signed__c, Lease_Docs_Signed_by_Homeowner__c, SystemModstamp, Lease_Docs_Signed_by_Homeowner_Date__c, 
                         Lease_Docs_Signed_by_NRG_SunLease__c, Lease_ID__c, Meter_State__c, Status__c
FROM            dbo.Allocation_Package__c AS ap
WHERE        (Facility_Allocation__c IN
                             (SELECT        Id
                               FROM            dbo.Facility_Allocation__c
                               WHERE        (CreatedDate > DATEADD(YEAR, - 1, GETDATE()))))


GO

