﻿USE [NRGHomeSolarSF]
GO

/****** Object:  View [dbo].[v_CSVR_Lead]    Script Date: 12/22/2017 4:33:14 PM ******/
DROP VIEW [dbo].[v_CSVR_Lead]
GO

/****** Object:  View [dbo].[v_CSVR_Lead]    Script Date: 12/22/2017 4:33:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/**** This view is for Community Solar Vendor Reports  ****/
CREATE VIEW [dbo].[v_CSVR_Lead]
AS
SELECT  TOP (100) PERCENT Community_Solar__c, CreatedById, CreatedDate, Do_Not_Call_Daytime_Internal__c, Do_Not_Call_National__c, Id, LastModifiedById, LastModifiedDate, Lead_18_Digit_ID__c, Lead_Channel__c, Lead_Owner_Name__c, Lead_Stage__c, LeadSource, Name, OwnerId, Phone, Phone_UID2__c, Sales_Representative__c, [Status], Total_Consumption__c
FROM dbo.Lead
WHERE Id IN
	(SELECT DISTINCT Id
		FROM (SELECT  Lead__c as [Id]
				FROM dbo.Facility_Allocation__c
                WHERE CreatedDate > DATEADD(YEAR, - 1, GETDATE())
             UNION ALL
              SELECT Id
				  FROM dbo.Lead AS ld
				  WHERE ((Community_Solar__c = 'true') OR (RecordTypeId = '012A00000012d1nIAA'))
						AND ld.CreatedDate > DATEADD(YEAR, - 1, GETDATE())
			  ) AS ids
	)


GO


