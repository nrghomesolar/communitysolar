﻿USE [NRGHomeSolarSF]
GO

/****** Object:  View [dbo].[v_CSVR_Facility_Allocation__c]    Script Date: 12/22/2017 7:02:11 PM ******/
DROP VIEW [dbo].[v_CSVR_Facility_Allocation__c]
GO

/****** Object:  View [dbo].[v_CSVR_Facility_Allocation__c]    Script Date: 12/22/2017 7:02:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[v_CSVR_Facility_Allocation__c]
AS
SELECT        TOP (100) PERCENT Active_Cancellation__c, Allocation_Package__c, Annual_Usage__c, Cancelled_Date__c, C_I__c , Co_Signer__c, CreatedById, CreatedDate, Credit_Status__c, 
                         Current_Net_Metering_Credit_Rate__c, Estimated_Production_Offset_Percent__c, Facility__c, Facility_Load_Zone__c, Facility_Utility__c, Id, Intent_To_Cancel__c, LastModifiedById, LastModifiedDate, Lead__c, 
                         Lease_Escalator__c, Lease_ID__c, Lease_Template_by_State__c, Meter_Address__c, Meter_City__c, Meter_Country__c, Meter_County__c, Meter_State__c, Meter_Street_1__c, 
                         Meter_Zip__c, Monthly_Payment__c, Months_Free__c, Premises_Number__c, Sales_Channel__c, Sales_Team__c, Salesperson__c, Secondary_Lead__c, Signer__c, Sold_Date__c, 
                         Sold_kW__c, Status__c, Welcome_Call_Performed__c, Welcome_Call_Type__c, Win_Back__c
FROM            dbo.Facility_Allocation__c 
WHERE        (CreatedDate > DATEADD(year, - 1, GETDATE()))
